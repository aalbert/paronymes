{
  inputs = {
    flake-utils.url = "github:numtide/flake-utils";
    nixpkgs.url = "nixpkgs/nixos-unstable";
  };

  outputs = { self, flake-utils, nixpkgs }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs { inherit system; };
        python = pkgs.python311;
        packages = pkgs.python311Packages;
      in {
        devShells.default = pkgs.mkShell {
          name = "paronymes-dev";
          packages = [ python ];
        };
      });
}
