var wordElm = document.getElementById("word");
var execBtn = document.getElementById("execute");
var outputElm = document.getElementById('output');
var errorElm = document.getElementById('error');
var phonemeElm1 = document.getElementById("phoneme1");
var phonemeElm2 = document.getElementById("phoneme2");
var opt_exElm = document.getElementById("display_ex");
var opt_authorElm = document.getElementById("display_author");
var opt_phonemeElm = document.getElementById("display_phoneme");
var opt_dialecteElm = document.getElementById("display_dialecte");

// Print an error
function error(e) {
	console.log(e);
	errorElm.style.height = '2em';
	errorElm.textContent = e.message;
}

// Create a query from the html elements
function makeQuery() {
  const phoneme = (opt_phonemeElm.checked) ? ", phoneme1, phoneme2" : "";
  const dialecte = (opt_dialecteElm.checked) ? ", dialecte" : "";
  const exemple1 = (opt_exElm.checked) ? ", data" : "";
  const exemple2 = (opt_exElm.checked) ? "LEFT JOIN contrepetries ON paronymes.id = paro1_id OR paronymes.id = paro2_id" : "";
  const auteur = (opt_exElm.checked && opt_authorElm.checked) ? ", auteur" : "";
  const where = (wordElm.value != "" || phonemeElm1.value != "" || phonemeElm2.value != "") ? " WHERE true" : "";
  const input_word = (wordElm.value != "") ? " AND (mot1 LIKE '%"+wordElm.value+"%' OR mot2 LIKE '%"+wordElm.value+"%')" : "";
  const input_phoneme1 = (phonemeElm1.value != "") ? " AND (phoneme1 LIKE '"+phonemeElm1.value+"' OR phoneme2 LIKE '"+phonemeElm1.value+"')" : "";
  const input_phoneme2 = (phonemeElm2.value != "") ? " AND (phoneme1 LIKE '"+phonemeElm2.value+"' OR phoneme2 LIKE '"+phonemeElm2.value+"')" : "";
  const query = "SELECT mot1, mot2"+phoneme+dialecte+exemple1+auteur+" FROM 'paronymes'"+exemple2+where+input_word+input_phoneme1+input_phoneme2;
  return query;
}

// Add a table cell
  function addCell(row, text, colspan) {
	var cell = document.createElement("th");
	if (colspan != ''){ cell.setAttribute("colspan", colspan); }
	var cellText = document.createTextNode(text);
	cell.appendChild(cellText);
	row.appendChild(cell);
	return row;
}

// Create a table header from html elements
function makeHeader() {
  var row = document.createElement("tr");
  row = addCell(row, "paronyme", "2");
  if (opt_phonemeElm.checked){ row = addCell(row, "phonèmes", "2"); }
  if (opt_dialecteElm.checked){ row = addCell(row, "dialecte", ''); }
  if (opt_exElm.checked){ row = addCell(row, "contrepetrie", ''); }
  if (opt_exElm.checked && opt_authorElm.checked){ row = addCell(row, "auteur", ''); }
  return row;
}

// Load the sqlite module
window.sqlite3InitModule().then(function(sqlite3){

  // Load the database
	async function loadDB(path) {

	  // Fetch the database from the server
  	const response = await fetch(path);
  	const buf = await response.arrayBuffer();

	  // Load the database from its serialization
	  const bytes = new Uint8Array(buf);
  	const p = sqlite3.wasm.allocFromTypedArray(bytes);
	  const db = new sqlite3.oo1.DB();
	  sqlite3.capi.sqlite3_deserialize(
    	db.pointer,
    	"main",
    	p,
    	bytes.length,
    	bytes.length,
    	sqlite3.capi.SQLITE_DESERIALIZE_FREEONCLOSE
  	);
	  return db;
	}
  loadDB("data.sqlite3").then(function(db){

    // Close the database before exiting
    function closingCode(){
 	    db.close();
      return null;
    }
    window.onbeforeunload = closingCode;

    // Query the database when the button is clicked
    function exec() {
      try {

		    outputElm.innerHTML = "";
		    var tbl = document.createElement("table");
		    outputElm.appendChild(tbl);
		    tbl.appendChild(makeHeader());

        db.exec({
		      sql: makeQuery(),
          rowMode: 'array',
          callback: function(values){

			      var row = document.createElement("tr");
			      values.forEach(function(val){
   			 	    var cell = document.createElement("td");
			  	    if (val == null){ val = ""; }
			  	    var cellText = document.createTextNode(val);
			  	    cell.appendChild(cellText);
				      row.appendChild(cell);
			      });
			      tbl.appendChild(row);
          }
        });
      } catch(e) {
        error(e);
      }
    }
    execBtn.addEventListener("click", exec, true);

  }, error);

}, error);
