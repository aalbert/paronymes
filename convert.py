import csv, sqlite3

# open database
con = sqlite3.connect("website/data.sqlite3")
cur = con.cursor()
cur.execute("CREATE TABLE IF NOT EXISTS mots (id TEXT PRIMARY KEY) WITHOUT ROWID")
cur.execute("CREATE TABLE IF NOT EXISTS phonemes (id TEXT PRIMARY KEY) WITHOUT ROWID")
cur.execute(
    "CREATE TABLE IF NOT EXISTS paronymes (id INTEGER PRIMARY KEY AUTOINCREMENT, mot1 TEXT NOT NULL, mot2 TEXT NOT NULL, phoneme1 TEXT NOT NULL, phoneme2 TEXT NOT NULL, dialecte TEXT, UNIQUE(mot1, mot2, phoneme1))"
)
cur.execute(
    "CREATE TABLE IF NOT EXISTS contrepetries (id INTEGER PRIMARY KEY AUTOINCREMENT, paro1_id INTEGER NOT NULL, paro2_id INTEGER NOT NULL, data TEXT NOT NULL, auteur TEXT, UNIQUE(paro1_id, paro2_id, data))"
)
cur.execute(
    """CREATE TRIGGER IF NOT EXISTS sort_before_insert_paro
    BEFORE INSERT ON paronymes
    WHEN NEW.mot1 > NEW.mot2
    BEGIN
        SELECT RAISE(ABORT,'mots non triés');
    END"""
)
cur.execute(
    """CREATE TRIGGER IF NOT EXISTS sort_before_insert_contre
    BEFORE INSERT ON contrepetries
    WHEN NEW.paro1_id > NEW.paro2_id
    BEGIN
        SELECT RAISE(ABORT,'ids non triés');
    END"""
)


def deco(text):
    return '("' + text + '")'


def insert_mot(mot):
    cur.execute("INSERT OR IGNORE INTO mots(id) VALUES" + deco(mot))


def insert_phoneme(phoneme):
    cur.execute("INSERT OR IGNORE INTO phonemes(id) VALUES" + deco(phoneme))


def insert_paro(row):
    if row[0] > row[1]:
        row[0], row[1], row[2], row[3] = row[1], row[0], row[3], row[2]
    cur.execute(
        "INSERT OR REPLACE INTO paronymes(mot1, mot2, phoneme1, phoneme2, dialecte) VALUES"
        + deco('","'.join(row))
    )
    return cur.lastrowid


def insert_contre(paro1_id, paro2_id, row):
    if paro1_id > paro2_id:
        paro1_id, paro2_id = paro2_id, paro1_id
    cur.execute(
        'INSERT OR REPLACE INTO contrepetries(paro1_id, paro2_id, data, auteur) VALUES({},{},"{}","{}")'.format(
            paro1_id, paro2_id, row[6], row[7]
        )
    )


# process paronymes.csv
with open("data/paronymes.csv", "r") as csvfile:
    reader = csv.reader(csvfile, delimiter=",")
    next(reader)
    for row in reader:
        insert_mot(row[0])
        insert_mot(row[1])
        insert_phoneme(row[2])
        insert_phoneme(row[3])
        insert_paro(row)

with open("data/contrepetries.csv", "r") as csvfile:
    reader = csv.reader(csvfile, delimiter=",")
    next(reader)
    for row in reader:
        insert_mot(row[0])
        insert_mot(row[1])
        insert_mot(row[2])
        insert_mot(row[3])
        insert_phoneme(row[4])
        insert_phoneme(row[5])
        paro1_id = insert_paro([row[0], row[1], row[4], row[5], row[8]])
        paro2_id = insert_paro([row[2], row[3], row[4], row[5], row[8]])
        insert_contre(paro1_id, paro2_id, row)

# close database
con.commit()
con.close()


# test database
con = sqlite3.connect("website/data.sqlite3")
cur = con.cursor()
print(cur.execute("SELECT * FROM mots").fetchall())
print(cur.execute("SELECT * FROM phonemes").fetchall())
print(cur.execute("SELECT * FROM paronymes").fetchall())
print(cur.execute("SELECT * FROM contrepetries").fetchall())
con.close()
